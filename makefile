CFLAGS+=-fPIE -I /usr/local/include/lua54 -Wall -fdiagnostics-color=never
PREFIX?=/local

nativebuf.so:	nativebuf.o
	$(CC) $(CFLAGS) -shared -o $@ nativebuf.o
install:	nativebuf.so
	strip nativebuf.so
	cp nativebuf.so $(PREFIX)/lib/lua/5.4/nativebuf.so
check:	nativebuf.so
	lua54 test.lua
clean:
	rm -f *.o *.so
