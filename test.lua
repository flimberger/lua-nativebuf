nativebuf = require "nativebuf"
local buf = nativebuf.alloc(64)
print(buf)
assert(#buf == 64, string.format("expected 64, got %s instead", #buf))
print(string.format("0x%x", buf:getptr()))
for i = 1, 10 do
  buf[i] = i
end
buf:setbytes("test", 10)
buf:setbytes("hi")
for i = 1, 20 do
  print(i, buf[i])
end
local b = buf:getbytes()
local s = buf:getbytes(10, 4)
assert(s == "test", string.format('expected "test", got "%s" instead', s))
s = buf:getbytes(60)
assert(#s == 5, string.format("expected 5, got %d instead", #s))
