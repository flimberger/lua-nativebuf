#include <stdlib.h>
#include <string.h>

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

struct Nativebuf
{
	size_t size;
	unsigned char *data;
};
typedef struct Nativebuf	Nativebuf;

#define METATABLENAME		"nativebuf.Nativebuf"
#define checknativebuf(s)	(Nativebuf*)luaL_checkudata(s, 1, METATABLENAME)

static int
alloc(lua_State *s)
{
	Nativebuf *buf;
	size_t	n;

	n = luaL_checkinteger(s, 1);
	luaL_argcheck(s, n >= 1, 1, "invalid size");
	buf = (Nativebuf*)lua_newuserdata(s, sizeof(*buf));
	buf->size = n;
	buf->data = calloc(n, 1);
	if(!buf->data)
		luaL_error(s, "allocation failed");
	luaL_getmetatable(s, METATABLENAME);
	lua_setmetatable(s, -2);
	return 1;
}

static int
freebuf(lua_State *s)
{
	Nativebuf *buf;

	buf = checknativebuf(s);
	if(buf->data)
		free(buf->data);
	return 0;
}

static int
bufsize(lua_State *s)
{
	Nativebuf *buf;

	buf = checknativebuf(s);
	lua_pushinteger(s, buf->size);
	return 1;
}

static int
tostring(lua_State *s)
{
	Nativebuf *buf;

	buf = checknativebuf(s);
	lua_pushfstring(s, "nativebuf: %p", (void*)buf->data);
	return 1;
}

static int
getidx(lua_State *s)
{
	Nativebuf *buf;
	size_t i;

	buf = checknativebuf(s);
	/* string keys need to be intercepted and forwarded to the metatable */
	if(lua_type(s, 2) == LUA_TSTRING){
		luaL_getmetatable(s, METATABLENAME);
		lua_pushvalue(s, 2);
		if(lua_gettable(s, -2) != LUA_TFUNCTION)
			luaL_error(s, "invalid function");
		return 1;
	}
	i = luaL_checkinteger(s, 2) - 1;
	if(i > buf->size)
		luaL_error(s, "index out of range");
	lua_pushinteger(s, buf->data[i]);
	return 1;
}

static int
setidx(lua_State *s)
{
	Nativebuf *buf;
	size_t i;
	char c;

	buf = checknativebuf(s);
	i = luaL_checkinteger(s, 2) - 1;
	c = luaL_checkinteger(s, 3);
	if(i > buf->size)
		luaL_error(s, "index out of range");
	buf->data[i] = c;
	return 0;
}

static int
getptr(lua_State *s)
{
	Nativebuf *buf;

	buf = checknativebuf(s);
	lua_pushinteger(s, (intptr_t)buf->data);
	return 1;
}

static int
getbytes(lua_State *s)
{
	Nativebuf *buf;
	size_t off, len;

	buf = checknativebuf(s);
	switch(lua_gettop(s)){
	default:
		off = 0;
		len = buf->size;
		break;
	case 2:
		off = luaL_checkinteger(s, 2) - 1;
		len = buf->size - off;
		break;
	case 3:
		off = luaL_checkinteger(s, 2) - 1;
		len = luaL_checkinteger(s, 3);
	}
	if(off > buf->size)
		luaL_error(s, "offset out of range");
	if(len > buf->size - off)
		len = buf->size - off;	/* truncate result */
	lua_pushlstring(s, (char*)(buf->data + off), len);
	return 1;
}

static int
setbytes(lua_State *s)
{
	Nativebuf *buf;
	size_t off, len;
	const char *input;

	buf = checknativebuf(s);
	input = luaL_checklstring(s, 2, &len);
	off = 0;
	if(lua_gettop(s) > 2){
		off = luaL_checkinteger(s, 3) - 1;
	}
	if(off > buf->size)
		luaL_error(s, "offset out of range");
	if(len > buf->size - off)
		luaL_error(s, "not enough space");
	memcpy(buf->data + off, input, len);
	return 0;
}

static struct luaL_Reg methods[] = {
	{"__gc", freebuf},
	{"__len", bufsize},
	{"__tostring", tostring},
	{"__index", getidx},
	{"__newindex", setidx},
	{"getptr", getptr},
	{"getbytes", getbytes},
	{"setbytes", setbytes},
	{NULL, NULL},
};

static struct luaL_Reg modfuncs[] = {
	{"alloc", alloc},
	{NULL, NULL},
};

int
luaopen_nativebuf(lua_State *s)
{
	luaL_newmetatable(s, METATABLENAME);
	luaL_setfuncs(s, methods, 0);
	luaL_newlib(s, modfuncs);
	return 1;
}
